# Tamara Data Engineer Coding Challenge

## Requirements
The payload for order event in our application is in json format. So when we need query/aggregate fields in JSON column, it will take a considerable time. And we can't add index to fields inside JSON column to speedup the query. So we need to create some pipelines to denormalize JSON field in order_events tables to other columns/tables.

- Setup project structure + Docker + code linting + mypy
- Create Unit test + Integration test: For critical logic
- Storage: MySQL

Good to have:
- Architecture: Cloud native (K8s)
- Define CICD
